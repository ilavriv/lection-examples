import { counterRoutine } from './routines';

export default (state = 0, action) => {
  switch(action.type) {
    case counterRoutine.INCREMENT:
      return state + 1;
    case counterRoutine.DECREMENT:
      return state - 1;
    default:
      return state;
  }
};