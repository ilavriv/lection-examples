import { createRoutineCreator } from 'redux-saga-routines';

const createCounterRoutine = createRoutineCreator([ 'TRIGGER', 'INCREMENT', 'DECREMENT']);

export const counterRoutine = createCounterRoutine('counter');