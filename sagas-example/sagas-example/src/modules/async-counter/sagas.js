import { put, takeLatest, call, all } from 'redux-saga/effects';

import { counterRoutine } from './routines';

import { delay } from './actions';

function* incrementAsync() {
  yield delay();
  yield put(counterRoutine.increment());
}

export default function* () {
  yield takeLatest(counterRoutine.TRIGGER, incrementAsync)
}