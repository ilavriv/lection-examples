import { createStore, applyMiddleware } from 'redux';
import { all } from 'redux-saga/effects';
import createSagaMiddleware from 'redux-saga';

import counterReducer from './modules/async-counter/reducers';

import counterSaga from './modules/async-counter/sagas';

const sagaMiddleware = createSagaMiddleware();

function *rootSaga() {
  return yield all([
    counterSaga()
  ])
}

export default () => {
  const store = createStore(counterReducer, applyMiddleware(sagaMiddleware));

  sagaMiddleware.run(rootSaga);

  return store;
}
