import React from 'react';
import { Provider } from 'react-redux';
import './App.css';

import configureStore from './store';
import Counter from './Counter';

function App() {
  return (
    <Provider store={configureStore()}>
      <Counter />
    </Provider>
  );
}

export default App;
