import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import AsyncCouter from './modules/async-counter/components/AsyncCounter';
import { counterRoutine } from './modules/async-counter/routines';

const mapStateToProps = (state) => ({
  count: state
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  increment: counterRoutine.trigger,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AsyncCouter);